package com.socialsupacrew.meditation.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.meditation.R
import com.socialsupacrew.meditation.ui.AudioListViewModel.MeditationListEntity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.audio_list_item.audioDuration
import kotlinx.android.synthetic.main.audio_list_item.audioName
import kotlinx.android.synthetic.main.audio_list_item.background
import kotlinx.android.synthetic.main.audio_list_item.container
import kotlinx.android.synthetic.main.audio_list_item.isSelected
import kotlinx.android.synthetic.main.audio_section_item.sectionTitle

sealed class AudioListHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    abstract fun bind(entity: MeditationListEntity, actionModeEnabled: Boolean)
}

class AudioSectionHolder(
    override val containerView: View
) : AudioListHolder(containerView) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.audio_section_item, parent, false)
    )

    override fun bind(entity: MeditationListEntity, actionModeEnabled: Boolean) {
        entity as MeditationListEntity.SectionEntity
        sectionTitle.setText(entity.title)
    }
}

class AudioFileHolder(
    override val containerView: View,
    private val listener: Listener
) : AudioListHolder(containerView) {

    constructor(parent: ViewGroup, listener: Listener) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.audio_list_item, parent, false),
        listener
    )

    override fun bind(entity: MeditationListEntity, actionModeEnabled: Boolean) {
        entity as MeditationListEntity.AudioEntity
        background.setImageResource(entity.illustration)
        audioName.text = entity.displayName ?: entity.name
        audioDuration.text = entity.duration
        isSelected.isVisible = entity.isSelected

        container.setOnClickListener {
            listener.onAudioFileClicked(entity.id, entity.isSelected, it)
        }

//        container.setOnLongClickListener {
//            if (actionModeEnabled) return@setOnLongClickListener false
//            listener.onAudioFileLongClicked(entity.id, entity.displayName ?: entity.name)
//        }
    }

    interface Listener {
        fun onAudioFileClicked(id: Long, isSelected: Boolean, view: View)
        fun onAudioFileLongClicked(id: Long, name: String): Boolean
    }
}

class AudioFooterHolder(
    override val containerView: View
) : AudioListHolder(containerView) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.audio_footer_item, parent, false)
    )

    override fun bind(entity: MeditationListEntity, actionModeEnabled: Boolean) {
        entity as MeditationListEntity.FooterEntity
    }
}
