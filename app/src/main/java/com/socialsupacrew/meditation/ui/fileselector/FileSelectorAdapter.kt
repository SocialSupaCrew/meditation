package com.socialsupacrew.meditation.ui.fileselector

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.socialsupacrew.meditation.core.BaseAdapter
import com.socialsupacrew.meditation.ui.fileselector.FileSelectorAdapter.ViewType.FILE
import com.socialsupacrew.meditation.ui.fileselector.FileSelectorViewModel.AudioDisplay


class FileSelectorAdapter(
    private val listener: FileSelectorHolder.Listener
) : BaseAdapter<AudioDisplay, FileSelectorHolder>(
    DIFF_CALLBACK
) {

    override fun getItemViewType(position: Int): Int {
        return FILE.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileSelectorHolder {
        return when (viewType) {
            FILE.id -> FileSelectorHolder(
                parent,
                listener
            )
            else -> throw IllegalArgumentException("$viewType is not supported")
        }
    }

    override fun onBindViewHolder(holder: FileSelectorHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private enum class ViewType {
        FILE;

        val id = ordinal
    }

    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<AudioDisplay> =
            object : DiffUtil.ItemCallback<AudioDisplay>() {
                override fun areItemsTheSame(oldItem: AudioDisplay, newItem: AudioDisplay) =
                    oldItem.id == newItem.id

                override fun areContentsTheSame(oldItem: AudioDisplay, newItem: AudioDisplay) =
                    oldItem.hashCode() == newItem.hashCode()
            }
    }
}
