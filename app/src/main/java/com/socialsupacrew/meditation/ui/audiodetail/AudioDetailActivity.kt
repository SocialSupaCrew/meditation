package com.socialsupacrew.meditation.ui.audiodetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.socialsupacrew.meditation.R
import com.socialsupacrew.meditation.core.BaseActivity
import com.socialsupacrew.meditation.data.event.AudioStateBus
import com.socialsupacrew.meditation.data.event.MediaControllerBus
import com.socialsupacrew.meditation.ui.audiodetail.AudioDetailViewModel.AudioDetailEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_audio_detail.audioName
import kotlinx.android.synthetic.main.activity_audio_detail.illustration
import kotlinx.android.synthetic.main.activity_audio_detail.playBtn
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class AudioDetailActivity : BaseActivity() {

    private val id by lazy { intent.getLongExtra(EXTRA_AUDIO_ID, 0) }

    private val viewModel: AudioDetailViewModel by viewModel { parametersOf(id) }
    private val audioStateBus: AudioStateBus by inject()
    private val mediaControllerBus: MediaControllerBus by inject()

    private var audioStateDisposable: Disposable? = null

    private var isPlaying: Boolean = false

    private var currentState: State? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_detail)

        viewModel.getStateLiveData().observe(this, Observer { applyState(it) })

        audioStateDisposable?.dispose()
        audioStateDisposable = audioStateBus.observable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ isPlaying ->
                this.isPlaying = isPlaying
                if (isPlaying) {
                    playBtn.icon = getDrawable(R.drawable.ic_pause_black_24dp)
                } else {
                    playBtn.icon = getDrawable(R.drawable.ic_play_arrow_black_24dp)
                }
            }, { Log.e(TAG, "", it) })

        playBtn.setOnClickListener {
            mediaControllerBus.isPlaying = !isPlaying
        }
    }

    private fun applyState(state: State) {
        if (state.audio != null) {
            setupAudio(state.audio)
        }
        currentState = state
    }

    private fun setupAudio(audio: AudioDetailEntity) {
        startService(MediaService.newIntent(this, audio.id, audio.name, audio.uri))
        audioName.text = audio.displayName ?: audio.name
        illustration.setImageResource(audio.illustration)
    }

    companion object {
        private const val EXTRA_AUDIO_ID = "extra:audioId"
        private const val TAG = "AudioDetailActivity"

        fun newIntent(context: Context, audioId: Long): Intent {
            return Intent(context, AudioDetailActivity::class.java).apply {
                putExtra(EXTRA_AUDIO_ID, audioId)
            }
        }
    }
}
