package com.socialsupacrew.meditation.ui.fileselector

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.meditation.R
import com.socialsupacrew.meditation.core.BaseActivity
import com.socialsupacrew.meditation.design.showSnackbar
import kotlinx.android.synthetic.main.activity_file_selector.audioList
import kotlinx.android.synthetic.main.activity_file_selector.root
import kotlinx.android.synthetic.main.activity_file_selector.topAppBar
import org.koin.android.viewmodel.ext.android.viewModel

class FileSelectorActivity : BaseActivity() {

    private val viewModel: FileSelectorViewModel by viewModel()

    private val adapter by lazy { FileSelectorAdapter(viewModel) }

    private var currentState: State? = null

    private val projection = arrayOf(
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.DISPLAY_NAME,
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.SIZE
    )

    // Show only audios that are at least 5 minutes in duration.
//    private val selection = MediaStore.Audio.Media.DURATION
//    private val selectionArgs = arrayOf(
//        TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES).toString()
//    )

    // Display audios in alphabetical order based on their display name.
    private val sortOrder = "${MediaStore.Audio.Media.DISPLAY_NAME} ASC"

    private val query by lazy {
        contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            projection,
            null,
            null,
            sortOrder
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_file_selector)

        viewModel.getStateLiveData().observe(this, Observer { applyState(it) })

        topAppBar.setNavigationOnClickListener {
            finish()
        }

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = RecyclerView.VERTICAL
//        val itemDecoration = CustomDividerItem(this, layoutManager.orientation)
//        audioList.addItemDecoration(itemDecoration)
        audioList.layoutManager = layoutManager
        audioList.adapter = adapter

        if (checkSelfPermission(READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(READ_EXTERNAL_STORAGE), REQUEST_CODE_STORAGE_PERMISSION
            )
        } else {
            viewModel.fetchAudio(query)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                viewModel.fetchAudio(query)
            } else {
                root.showSnackbar("Permission is required to access audio files")
            }
        }
    }

    override fun onDestroy() {
        query?.close()
        viewModel.onDestroy()
        super.onDestroy()
    }

    private fun applyState(state: State) {
        if (currentState?.audioDisplay != state.audioDisplay && state.audioDisplay.isNotEmpty()) {
            adapter.items = state.audioDisplay
        }

        currentState = state
    }

    companion object {
        private const val REQUEST_CODE_STORAGE_PERMISSION = 1

        fun newIntent(context: Context) = Intent(context, FileSelectorActivity::class.java)
    }
}
