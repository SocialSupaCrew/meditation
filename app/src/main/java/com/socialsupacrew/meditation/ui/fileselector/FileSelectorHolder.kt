package com.socialsupacrew.meditation.ui.fileselector

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.meditation.R
import com.socialsupacrew.meditation.ui.fileselector.FileSelectorViewModel.AudioDisplay
import com.socialsupacrew.meditation.ui.fileselector.FileSelectorViewModel.AudioEntity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.audio_file_item.audioDuration
import kotlinx.android.synthetic.main.audio_file_item.audioIsSelected
import kotlinx.android.synthetic.main.audio_file_item.audioName
import kotlinx.android.synthetic.main.audio_file_item.container

class FileSelectorHolder(
    override val containerView: View,
    private val listener: Listener
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    constructor(parent: ViewGroup, listener: Listener) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.audio_file_item, parent, false),
        listener
    )

    fun bind(entity: AudioDisplay) {
        audioName.text = entity.audioEntity.name
        audioDuration.text = entity.audioEntity.durationDisplay
        audioIsSelected.isVisible = entity.isSelected

        container.setOnClickListener {
            listener.onAudioFileClicked(entity.audioEntity, !entity.isSelected)
        }
    }

    interface Listener {
        fun onAudioFileClicked(entity: AudioEntity, isSelected: Boolean)
    }
}
