package com.socialsupacrew.meditation.ui

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.meditation.R
import com.socialsupacrew.meditation.core.BaseActivity
import com.socialsupacrew.meditation.ui.audiodetail.AudioDetailActivity
import com.socialsupacrew.meditation.ui.fileselector.FileSelectorActivity
import kotlinx.android.synthetic.main.activity_audio_list.audioList
import kotlinx.android.synthetic.main.activity_audio_list.emptyState
import kotlinx.android.synthetic.main.activity_audio_list.fabAdd
import kotlinx.android.synthetic.main.activity_audio_list.topAppBar
import org.koin.android.viewmodel.ext.android.viewModel


class AudioListActivity : BaseActivity() {

    private val viewModel: AudioListViewModel by viewModel()
    private val adapter by lazy { AudioListAdapter(viewModel) }
//    private val vibrator by lazy { getSystemService(Context.VIBRATOR_SERVICE) as Vibrator }

    private var menu: Menu? = null
    private var actionModeCallback: AudioListActionModeCallback? = null

    private var currentState: State? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio_list)

        viewModel.getStateLiveData().observe(this, Observer { applyState(it) })

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = RecyclerView.VERTICAL
        audioList.layoutManager = layoutManager
        audioList.adapter = adapter

        setSupportActionBar(topAppBar)

        fabAdd.setOnClickListener {
            startActivity(FileSelectorActivity.newIntent(this))
        }

        audioList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (layoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    fabAdd.extend()
                } else {
                    fabAdd.shrink()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        return true
    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }

    private fun applyState(state: State) {
        if (state.audioList.isNotEmpty()) {
            adapter.items = state.audioList
            hideEmptyState()
        }

        if (state.audioList.isEmpty()) {
            adapter.items = emptyList()
            showEmptyState()
        }

        if (state.openAudio != null) {
            openAudio(state.openAudio, state.transition)
        }

        if (currentState?.actionModeEnable != state.actionModeEnable && state.actionModeEnable) {
            startActionMode(state.onlyOneSelected)
        }

        if (currentState?.actionModeEnable != state.actionModeEnable && !state.actionModeEnable) {
            hideActionMode()
        }

        if (state.actionModeEnable && currentState?.onlyOneSelected != state.onlyOneSelected) {
            updateActionMode(state.onlyOneSelected)
        }

        currentState = state
    }

    private fun hideEmptyState() {
        emptyState.isGone = true
        audioList.isVisible = true
    }

    private fun showEmptyState() {
        emptyState.isVisible = true
        audioList.isGone = true
    }

    private fun openAudio(id: Long, view: View?) {
        if (view != null) {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                view,
                TRANSITION_NAME
            )
            startActivity(AudioDetailActivity.newIntent(this, id), options.toBundle())
        } else {
            startActivity(AudioDetailActivity.newIntent(this, id))
        }
    }

    private fun startActionMode(onlyOneSelected: Boolean) {
        actionModeCallback = AudioListActionModeCallback().also {
            it.listener = viewModel
            it.startActionMode(
                this,
                R.menu.menu_audio_list_action_mode_full,
                getString(R.string.title_audio_file_action_mode_full)
            )
        }
        adapter.actionModeEnabled = true
    }

    private fun hideActionMode() {
        actionModeCallback?.finishActionMode()
        adapter.actionModeEnabled = false
    }

    private fun updateActionMode(onlyOneSelected: Boolean) {
        actionModeCallback?.updateActionMode(onlyOneSelected)
    }

    companion object {
        private const val MENU_ITEM_ID_EDIT_ITEM = 155
        private const val MENU_ITEM_ID_DELETE_ITEM = 156
        private const val TRANSITION_NAME = "illustration"
    }
}


//        val menu = menu ?: return
//        val menuItem = if (moreThanOneSelected) {
//            menu.add(
//                Menu.NONE,
//                MENU_ITEM_ID_DELETE_ITEM,
//                Menu.NONE,
//                R.string.edit_audio_file
//            )
//        } else {
//            menu.add(
//                Menu.NONE,
//                MENU_ITEM_ID_EDIT_ITEM,
//                Menu.NONE,
//                R.string.delete_audio_file
//            )
//        }
//        menuItem.setIcon(
//            if (moreThanOneSelected) R.drawable.ic_delete_black_24dp
//            else R.drawable.ic_mode_edit_black_24dp
//        )
//        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
