package com.socialsupacrew.meditation.ui.audiodetail

import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import androidx.core.net.toUri
import com.socialsupacrew.meditation.core.NOW_PLAYING_NOTIFICATION
import com.socialsupacrew.meditation.core.NotificationBuilder
import com.socialsupacrew.meditation.data.AudioFileRepository
import com.socialsupacrew.meditation.data.event.AudioStateBus
import com.socialsupacrew.meditation.data.event.MediaControllerBus
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class MediaService : Service() {

    private val audioStateBus: AudioStateBus by inject()
    private val mediaControllerBus: MediaControllerBus by inject()
    private val audioFileRepository: AudioFileRepository by inject()
    private val notificationBuilder: NotificationBuilder by inject()

    private val notificationManager by lazy { NotificationManagerCompat.from(applicationContext) }
    private val becomingNoisyReceiver by lazy { BecomingNoisyReceiver(applicationContext) }

    private val mediaPlayer = MediaPlayer()

    private val isPlaying: Boolean
        get() = mediaPlayer.isPlaying

    private lateinit var name: String
    private lateinit var uri: Uri

    private var mediaControllerDisposable: Disposable? = null

    private var id: Long = 0L
    private var isForegroundService = false

    override fun onCreate() {
        super.onCreate()

        mediaControllerDisposable?.dispose()
        mediaControllerDisposable = mediaControllerBus.observable()
            .subscribeOn(Schedulers.io())
            .subscribe({ isPlaying ->
                if (isPlaying) {
                    play()
                } else {
                    stop()
                }
            }, { Log.e(TAG, "", it) })
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.hasExtra(EXTRA_PLAY_PAUSE) == true) {
            if (intent.getBooleanExtra(EXTRA_PLAY_PAUSE, false)) {
                play()
            } else {
                stop()
            }
            return START_REDELIVER_INTENT
        }

        if (intent?.hasExtra(EXTRA_ID) == true) {
            id = intent.getLongExtra(EXTRA_ID, 0L)
        }

        if (intent?.hasExtra(EXTRA_NAME) == true) {
            name = intent.getStringExtra(EXTRA_NAME) ?: ""
        }

        if (intent?.hasExtra(EXTRA_URI) == true) {
            uri = (intent.getStringExtra(EXTRA_URI) ?: "").toUri()
        }

        start()
        return START_STICKY
    }

    override fun onDestroy() {
        audioStateBus.isPlaying = false
        mediaPlayer.release()
        mediaControllerDisposable?.dispose()
    }

    private fun start() {
        mediaPlayer.reset()
        mediaPlayer.setDataSource(applicationContext, uri)
        mediaPlayer.prepareAsync()
        mediaPlayer.isLooping = false
        mediaPlayer.setOnPreparedListener { isPrepared() }
        mediaPlayer.setOnCompletionListener { onCompletion() }
    }

    private fun play() {
        if (isPlaying) return
        mediaPlayer.start()
        audioStateBus.isPlaying = true
        updateNotification()
    }

    private fun stop() {
        if (!isPlaying) return
        mediaPlayer.pause()
        audioStateBus.isPlaying = false
        updateNotification()
    }

    private fun isPrepared() {
        mediaPlayer.start()
        audioStateBus.isPlaying = true
        updateNotification()
    }

    private fun onCompletion() {
        audioStateBus.isPlaying = false
        notificationManager.cancel(NOW_PLAYING_NOTIFICATION)
        stopForeground(true)

        Schedulers.io().scheduleDirect {
            audioFileRepository.markAsLastSeen(id)
        }
    }

    private fun updateNotification() {
        val notification = notificationBuilder.buildNotification(name, isPlaying)
        notificationManager.notify(NOW_PLAYING_NOTIFICATION, notification)

        if (isPlaying) {

            becomingNoisyReceiver.register()

            if (!isForegroundService) {
                startForeground(NOW_PLAYING_NOTIFICATION, notification)
                isForegroundService = true
            }
        } else {
            becomingNoisyReceiver.unregister()

            if (isForegroundService) {
                stopForeground(false)
                isForegroundService = false
            }
        }

    }

    companion object {
        private const val TAG = "MediaService"
        private const val EXTRA_ID = "extra:id"
        private const val EXTRA_NAME = "extra:name"
        private const val EXTRA_URI = "extra:uri"
        private const val EXTRA_PLAY_PAUSE = "extra:playPause"

        fun newIntent(context: Context, id: Long, name: String, uri: Uri): Intent {
            return Intent(context, MediaService::class.java).apply {
                putExtra(EXTRA_ID, id)
                putExtra(EXTRA_NAME, name)
                putExtra(EXTRA_URI, uri.toString())
            }
        }

        fun newIntent(context: Context, shouldPlay: Boolean): PendingIntent {
            return PendingIntent.getService(
                context,
                0,
                Intent(context, MediaService::class.java).apply {
                    putExtra(EXTRA_PLAY_PAUSE, shouldPlay)
                },
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        }
    }

    /**
     * Helper class for listening for when headphones are unplugged (or the audio
     * will otherwise cause playback to become "noisy").
     */
    inner class BecomingNoisyReceiver(
        private val context: Context
    ) : BroadcastReceiver() {

        private val noisyIntentFilter = IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)

        private var registered = false

        fun register() {
            if (!registered) {
                context.registerReceiver(this, noisyIntentFilter)
                registered = true
            }
        }

        fun unregister() {
            if (registered) {
                context.unregisterReceiver(this)
                registered = false
            }
        }

        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == AudioManager.ACTION_AUDIO_BECOMING_NOISY) {
                mediaPlayer.pause()
            }
        }
    }
}
