package com.socialsupacrew.meditation.ui

import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.socialsupacrew.meditation.R
import com.socialsupacrew.meditation.core.BaseViewModel
import com.socialsupacrew.meditation.data.AudioFileRepository
import com.socialsupacrew.meditation.data.local.AudioFile
import com.socialsupacrew.meditation.ui.AudioListViewModel.MeditationListEntity.AudioEntity
import com.socialsupacrew.meditation.ui.AudioListViewModel.MeditationListEntity.FooterEntity
import com.socialsupacrew.meditation.ui.AudioListViewModel.MeditationListEntity.SectionEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlin.math.round

class AudioListViewModel(
    private val repository: AudioFileRepository
) : BaseViewModel<State>(), AudioFileHolder.Listener, AudioListActionModeCallback.Listener {

    override val defaultState: State
        get() = State()

    private var audioFileDisposable: Disposable? = null

    private var actionModeEnable: Boolean = false
    private val selectedAudio: MutableList<Long> = mutableListOf()

    init {
        fetchSavedAudio()
    }

    override fun onAudioFileClicked(id: Long, isSelected: Boolean, view: View) {
        if (actionModeEnable) {
            if (isSelected) {
                selectedAudio.remove(id)
            } else {
                selectedAudio.add(id)
            }

            if (selectedAudio.isEmpty()) {
                reduce { it.copy(actionModeEnable = false) }
            }

            fetchSavedAudio()
        } else {
            reduce({ it.copy(openAudio = id, transition = view) },
                { it.copy(openAudio = null, transition = null) })
        }
    }

    override fun onAudioFileLongClicked(id: Long, name: String): Boolean {
        actionModeEnable = true
        selectedAudio.add(id)
        reduce { it.copy(actionModeEnable = true) }
        fetchSavedAudio()
        return true
    }

    override fun onActionItemClick(item: MenuItem) {
        when (item.itemId) {
            R.id.menu_delete_single,
            R.id.menu_delete_multi -> delete()
            R.id.menu_edit -> openEdit()
        }
    }

    override fun onDestroyActionMode() {
        actionModeEnable = false
        selectedAudio.clear()
        fetchSavedAudio()
        reduce { it.copy(actionModeEnable = false) }
    }

    private fun fetchSavedAudio() {
        audioFileDisposable?.dispose()
        audioFileDisposable = repository.getAllAsync()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { computeList(it) },
                { Log.e(TAG, "error fetchSavedAudio()", it) })
    }

    private fun computeList(result: List<AudioFile>) {
        val meditationList = mutableListOf<MeditationListEntity>()
        val audioEntities = toEntities(result)
        val lastPlayed = audioEntities.firstOrNull { it.lastPlayed }

        if (lastPlayed != null && !actionModeEnable) {
            meditationList.add(SectionEntity(Long.MIN_VALUE, R.string.section_last_played))
            meditationList.add(lastPlayed)
            meditationList.add(SectionEntity(Long.MIN_VALUE + 1, R.string.section_other))
        }
        meditationList.addAll(audioEntities)
        if (audioEntities.isNotEmpty()) {
            meditationList.add(FooterEntity)
        }
        reduce { it.copy(audioList = meditationList) }
    }

    private fun toEntities(result: List<AudioFile>): List<AudioEntity> {
        return result.map {
            AudioEntity(
                it.id,
                selectedAudio.contains(it.id),
                it.name,
                it.displayName,
                computeDuration(it.duration),
                it.lastPlayed,
                it.illustration
            )
        }
    }

    private fun computeDuration(duration: Int): String {
        // Round to nearest hundred
        val duration2 = (round(duration / 100F) * 100).toInt()
        val minute = duration2 / 1000 / 60
        val second = duration2 / 1000 % 60

        return String.format("%02d:%02d", minute, second)
    }

    private fun delete() {
        Schedulers.io().scheduleDirect {
            repository.delete(selectedAudio)
        }
    }

    private fun openEdit() {

    }

    internal fun onDestroy() {
        audioFileDisposable?.dispose()
    }

    sealed class MeditationListEntity(
        open val id: Long,
        open val isSelected: Boolean
    ) {
        data class SectionEntity(
            override val id: Long,
            @StringRes val title: Int
        ) : MeditationListEntity(id, false)

        data class AudioEntity(
            override val id: Long,
            override val isSelected: Boolean,
            val name: String,
            val displayName: String?,
            val duration: String,
            val lastPlayed: Boolean,
            @DrawableRes val illustration: Int
        ) : MeditationListEntity(id, isSelected)

        object FooterEntity : MeditationListEntity(Long.MAX_VALUE, false)
    }

    companion object {
        private const val TAG = "AudioListViewModel"
    }
}

data class State(
    val audioList: List<AudioListViewModel.MeditationListEntity> = emptyList(),
    val openAudio: Long? = null,
    val transition: View? = null,
    val oldName: String = "",
    val actionModeEnable: Boolean = false
) {
    val onlyOneSelected = audioList.filter { it.isSelected }.size == 1
}
