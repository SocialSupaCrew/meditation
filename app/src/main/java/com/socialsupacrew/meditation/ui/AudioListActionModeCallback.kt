package com.socialsupacrew.meditation.ui

import android.view.Menu
import android.view.MenuItem
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import com.socialsupacrew.meditation.R

class AudioListActionModeCallback : ActionMode.Callback {
    var listener: Listener? = null

    private var menu: Menu? = null
    private var mode: ActionMode? = null

    var isFinished = true

    @MenuRes
    private var menuResId: Int = 0
    private var title: String? = null
    private var subtitle: String? = null

    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        this.menu = menu
        this.mode = mode
        mode.title = title
        mode.subtitle = subtitle
        isFinished = false
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
        return false
    }

    override fun onDestroyActionMode(mode: ActionMode) {
        listener?.onDestroyActionMode()
        this.mode = null
    }

    override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
        listener?.onActionItemClick(item)
        mode.finish()
        return true
    }

    fun startActionMode(
        activity: AppCompatActivity,
        @MenuRes menuResId: Int = R.menu.menu_audio_list_action_mode_full,
        title: String? = null,
        subtitle: String? = null
    ) {
        this.menuResId = menuResId
        this.title = title
        this.subtitle = subtitle
        activity.startSupportActionMode(this)
    }

    fun updateActionMode(onlyOneSelected: Boolean) {
        menu?.clear()
        if (onlyOneSelected) {
            mode?.menuInflater?.inflate(R.menu.menu_audio_list_action_mode_full, menu)
            mode?.setTitle(R.string.title_audio_file_action_mode_full)
        } else {
            mode?.menuInflater?.inflate(R.menu.menu_audio_list_action_mode_delete, menu)
            mode?.setTitle(R.string.title_audio_file_action_mode_delete)
        }
    }

    fun finishActionMode() {
        mode?.finish()
        isFinished = true
    }

    interface Listener {
        fun onActionItemClick(item: MenuItem)
        fun onDestroyActionMode()
    }
}
