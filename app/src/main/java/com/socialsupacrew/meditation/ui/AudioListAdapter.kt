package com.socialsupacrew.meditation.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.socialsupacrew.meditation.core.BaseAdapter
import com.socialsupacrew.meditation.ui.AudioListAdapter.ViewType.FILE
import com.socialsupacrew.meditation.ui.AudioListAdapter.ViewType.FOOTER
import com.socialsupacrew.meditation.ui.AudioListAdapter.ViewType.SECTION
import com.socialsupacrew.meditation.ui.AudioListViewModel.MeditationListEntity

class AudioListAdapter(
    private val listener: AudioFileHolder.Listener
) : BaseAdapter<MeditationListEntity, AudioListHolder>(
    DIFF_CALLBACK
) {

    var actionModeEnabled: Boolean = false

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is MeditationListEntity.SectionEntity -> SECTION.id
            is MeditationListEntity.AudioEntity -> FILE.id
            is MeditationListEntity.FooterEntity -> FOOTER.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AudioListHolder {
        return when (viewType) {
            SECTION.id -> AudioSectionHolder(parent)
            FILE.id -> AudioFileHolder(parent, listener)
            FOOTER.id -> AudioFooterHolder(parent)
            else -> throw IllegalArgumentException("$viewType is not supported")
        }
    }

    override fun onBindViewHolder(holder: AudioListHolder, position: Int) {
        holder.bind(getItem(position), actionModeEnabled)
    }

    private enum class ViewType {
        SECTION,
        FILE,
        FOOTER;

        val id = ordinal
    }

    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<MeditationListEntity> =
            object : DiffUtil.ItemCallback<MeditationListEntity>() {
                override fun areItemsTheSame(
                    oldItem: MeditationListEntity,
                    newItem: MeditationListEntity
                ) =
                    oldItem.id == newItem.id

                override fun areContentsTheSame(
                    oldItem: MeditationListEntity,
                    newItem: MeditationListEntity
                ) =
                    oldItem.hashCode() == newItem.hashCode()
            }
    }
}
