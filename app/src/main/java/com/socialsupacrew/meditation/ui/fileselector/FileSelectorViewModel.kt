package com.socialsupacrew.meditation.ui.fileselector

import android.content.ContentUris
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.socialsupacrew.meditation.core.BaseViewModel
import com.socialsupacrew.meditation.data.AudioFileRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlin.math.round

class FileSelectorViewModel(
    private val repository: AudioFileRepository
) : BaseViewModel<State>(), FileSelectorHolder.Listener {
    override val defaultState: State
        get() = State()

    private var audioFileDisposable: Disposable? = null

    private val audioList: MutableList<AudioEntity> = mutableListOf()

    override fun onAudioFileClicked(entity: AudioEntity, isSelected: Boolean) {
        Schedulers.io().scheduleDirect {
            if (isSelected) {
                repository.insert(entity)
            } else {
                repository.delete(entity.id)
            }
            fetchSavedAudio()
        }
    }

    internal fun fetchAudio(query: Cursor?) {
        reduce { it.copy(isLoading = true) }

        query?.use { cursor ->
            // Cache column indices.
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
            val nameColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
            val durationColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
            val sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE)

            while (cursor.moveToNext()) {
                // Get values of columns for a given audio.
                val id = cursor.getLong(idColumn)
                val name = cursor.getString(nameColumn)
                val duration = cursor.getInt(durationColumn)
                val size = cursor.getInt(sizeColumn)

                val contentUri: Uri = ContentUris.withAppendedId(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    id
                )

                // Stores column values and the contentUri in a local object
                // that represents the media file.
                audioList += AudioEntity(
                    id,
                    contentUri,
                    name,
                    computeDuration(duration),
                    duration,
                    size
                )
            }

            fetchSavedAudio()
        }
    }

    private fun fetchSavedAudio() {
        audioFileDisposable?.dispose()
        audioFileDisposable = repository.getAllAsync()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                val savedIds = result.map { it.id }
                val audioDisplay = audioList.map {
                    AudioDisplay(
                        it.id,
                        it,
                        savedIds.contains(it.id)
                    )
                }
                reduce { it.copy(audioDisplay = audioDisplay, isLoading = false) }
            }, { Log.e(TAG, "error fetchSavedAudio()", it) })
    }

    private fun computeDuration(duration: Int): String {
        // Round to nearest hundred
        val duration2 = (round(duration / 100F) * 100).toInt()
        val minute = duration2 / 1000 / 60
        val second = duration2 / 1000 % 60

        return String.format("%02d:%02d", minute, second)
    }

    internal fun onDestroy() {
        audioFileDisposable?.dispose()
    }

    data class AudioDisplay(
        val id: Long,
        val audioEntity: AudioEntity,
        val isSelected: Boolean
    )

    data class AudioEntity(
        val id: Long,
        val uri: Uri,
        val name: String,
        val durationDisplay: String,
        val duration: Int,
        val size: Int
    )

    companion object {
        private const val TAG = "FileSelectorViewModel"
    }
}

data class State(
    val isLoading: Boolean = false,
    val audioDisplay: List<FileSelectorViewModel.AudioDisplay> = emptyList()
)
