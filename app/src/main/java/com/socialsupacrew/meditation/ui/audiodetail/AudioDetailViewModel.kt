package com.socialsupacrew.meditation.ui.audiodetail

import android.net.Uri
import android.util.Log
import androidx.annotation.DrawableRes
import com.socialsupacrew.meditation.core.BaseViewModel
import com.socialsupacrew.meditation.data.AudioFileRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class AudioDetailViewModel(
    private val repository: AudioFileRepository,
    private val audioId: Long
) : BaseViewModel<State>() {
    override val defaultState: State
        get() = State()

    private var audioDisposable: Disposable? = null

    init {
        getAudioDetail()
    }

    private fun getAudioDetail() {
        audioDisposable?.dispose()
        audioDisposable = repository.getAsync(audioId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ audioFile ->
                val audio = AudioDetailEntity(
                    audioFile.id,
                    audioFile.name,
                    audioFile.displayName,
                    audioFile.uri,
                    audioFile.duration,
                    audioFile.illustration
                )
                reduce { it.copy(audio = audio) }
            }, { Log.e(TAG, "error:getAudioDetail: ", it) })
    }

    data class AudioDetailEntity(
        val id: Long,
        val name: String,
        val displayName: String?,
        val uri: Uri,
        val duration: Int,
        @DrawableRes val illustration: Int
    )

    companion object {
        private const val TAG = "AudioDetailViewModel"
    }
}

data class State(
    val isLoading: Boolean = false,
    val audio: AudioDetailViewModel.AudioDetailEntity? = null
)
