package com.socialsupacrew.meditation

import android.app.Application
import androidx.room.Room
import com.socialsupacrew.meditation.core.NotificationBuilder
import com.socialsupacrew.meditation.data.AudioFileLocalDataSource
import com.socialsupacrew.meditation.data.AudioFileRepository
import com.socialsupacrew.meditation.data.event.AudioStateBus
import com.socialsupacrew.meditation.data.event.MediaControllerBus
import com.socialsupacrew.meditation.data.local.AppDatabase
import com.socialsupacrew.meditation.ui.AudioListViewModel
import com.socialsupacrew.meditation.ui.audiodetail.AudioDetailViewModel
import com.socialsupacrew.meditation.ui.fileselector.FileSelectorViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { FileSelectorViewModel(get()) }
    viewModel { AudioListViewModel(get()) }
    viewModel { (id: Long) -> AudioDetailViewModel(get(), id) }

    single { AudioFileRepository(get()) }
    single { AudioFileLocalDataSource(get()) }

    single { NotificationBuilder(get()) }

    single { AudioStateBus() }
    single { MediaControllerBus() }

    single { roomDataBase(get()) }
}

private fun roomDataBase(application: Application) =
    Room.databaseBuilder(application, AppDatabase::class.java, AppDatabase.DATABASE_NAME).build()
