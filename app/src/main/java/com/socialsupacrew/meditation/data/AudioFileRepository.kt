package com.socialsupacrew.meditation.data

import android.util.Log
import androidx.annotation.DrawableRes
import com.socialsupacrew.meditation.R
import com.socialsupacrew.meditation.data.AudioFileLocalDataSource.Illustration
import com.socialsupacrew.meditation.data.local.AppDatabase
import com.socialsupacrew.meditation.data.local.AudioFile
import com.socialsupacrew.meditation.ui.fileselector.FileSelectorViewModel

class AudioFileRepository constructor(private val local: AudioFileLocalDataSource) {
    private fun getAll() = local.getAll()

    fun getAllAsync() = local.getAllAsync()

    fun get(id: Long) = local.get(id)

    fun getAsync(id: Long) = local.getAsync(id)

    fun insert(audioFile: FileSelectorViewModel.AudioEntity) {
        local.insert(toEntity(audioFile))
    }

    fun delete(id: Long) {
        delete(listOf(id))
    }

    fun delete(ids: List<Long>) {
        Log.d("QSD", "ids size: ${ids.size}")
        local.delete(ids)
    }

    fun updateName(id: Long, newName: String) {
        val toUpdate = local.get(id)
        local.update(toUpdate.copy(displayName = newName))
    }

    fun updateIllustration(id: Long, @DrawableRes illustration: Int) {
        val toUpdate = local.get(id)
        local.update(toUpdate.copy(illustration = illustration))
    }

    fun markAsLastSeen(id: Long) {
        val all = getAll()
        all.forEach {
            if (it.id == id) {
                local.update(it.copy(lastPlayed = true))
            } else {
                local.update(it.copy(lastPlayed = false))
            }
        }
    }

    private fun toEntity(audioFile: FileSelectorViewModel.AudioEntity): AudioFile {
        return AudioFile(
            audioFile.id,
            audioFile.name,
            null,
            audioFile.duration,
            audioFile.size,
            audioFile.uri,
            false,
            getNextIllustration().illustration
        )
    }

    private fun getNextIllustration(): Illustration {
        return Illustration.getNextIllustration(local.getIllustrations())
    }
}

class AudioFileLocalDataSource constructor(private val database: AppDatabase) {

    fun getAll() = database.audioFileDao().getAll()

    fun getAllAsync() = database.audioFileDao().getAllAsync()

    fun get(id: Long) = database.audioFileDao().get(id)

    fun getIllustrations() = database.audioFileDao().getIllustrations()

    fun getAsync(id: Long) = database.audioFileDao().getAsync(id)

    fun insert(audioFile: AudioFile) {
        database.audioFileDao().insert(audioFile)
    }

    fun update(audioFile: AudioFile) {
        database.audioFileDao().insert(audioFile)
    }

    fun delete(ids: List<Long>) {
        database.audioFileDao().delete(ids)
    }

    @Suppress("unused")
    enum class Illustration(@DrawableRes val illustration: Int) {
        A(R.drawable.illustration_0),
        B(R.drawable.illustration_1),
        C(R.drawable.illustration_2),
        D(R.drawable.illustration_3),
        E(R.drawable.illustration_4),
        F(R.drawable.illustration_5),
        G(R.drawable.illustration_6),
        H(R.drawable.illustration_7),
        I(R.drawable.illustration_8),
        J(R.drawable.illustration_9),
        K(R.drawable.illustration_10);

        companion object {
            fun getNextIllustration(usedIllustration: List<Int>): Illustration {
                var illustration = values().firstOrNull {
                    !usedIllustration.contains(it.illustration)
                }

                if (illustration == null) {
                    illustration =
                        getByValue(
                            usedIllustration.groupingBy { it }
                                .eachCount()
                                .toList()
                                .sortedBy { (_, value) -> value }
                                .toMap()
                                .keys
                                .first()
                        )
                }

                return illustration
            }

            private fun getByValue(value: Int): Illustration {
                return values().first { it.illustration == value }
            }
        }
    }
}
