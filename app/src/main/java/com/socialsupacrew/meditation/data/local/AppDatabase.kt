package com.socialsupacrew.meditation.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [AudioFile::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun audioFileDao(): AudioFileDao

    companion object {
        const val DATABASE_NAME = "Meditation_database"
    }
}
