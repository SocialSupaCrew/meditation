package com.socialsupacrew.meditation.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface AudioFileDao {

    @Query("SELECT * FROM audioFile")
    fun getAll(): List<AudioFile>

    @Query("SELECT * FROM audioFile ORDER BY displayName ASC, name ASC")
    fun getAllAsync(): Flowable<List<AudioFile>>

    @Query("SELECT * FROM audioFile WHERE id = :id")
    fun get(id: Long): AudioFile

    @Query("SELECT illustration FROM audioFile")
    fun getIllustrations(): List<Int>

    @Query("SELECT * FROM audioFile WHERE id = :id")
    fun getAsync(id: Long): Single<AudioFile>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(audioFile: AudioFile)

    @Update
    fun update(audioFile: AudioFile)

    @Query("DELETE FROM audioFile WHERE id in (:ids)")
    fun delete(ids: List<Long>)
}
