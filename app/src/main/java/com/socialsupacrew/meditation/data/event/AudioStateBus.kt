package com.socialsupacrew.meditation.data.event

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

class AudioStateBus {
    private val bus: BehaviorSubject<Boolean> = BehaviorSubject.create()

    var isPlaying
        get() = bus.value!!
        set(value) {
            bus.onNext(value)
        }

    fun observable(): Observable<Boolean> = bus
}
