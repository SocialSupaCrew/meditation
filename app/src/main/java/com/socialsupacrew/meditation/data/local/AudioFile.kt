package com.socialsupacrew.meditation.data.local

import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "audioFile")
data class AudioFile(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "displayName") val displayName: String?,
    @ColumnInfo(name = "duration") val duration: Int,
    @ColumnInfo(name = "size") val size: Int,
    @ColumnInfo(name = "uri") val uri: Uri,
    @ColumnInfo(name = "lastPlayed") val lastPlayed: Boolean,
    @DrawableRes @ColumnInfo(name = "illustration") val illustration: Int
)
