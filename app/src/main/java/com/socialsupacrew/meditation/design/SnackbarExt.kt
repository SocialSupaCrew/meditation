package com.socialsupacrew.meditation.design

import android.view.View
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.socialsupacrew.meditation.R

fun View.showSnackbar(
    @StringRes snackbarText: Int,
    timeLength: Int = Snackbar.LENGTH_LONG,
    snackbarAction: BkSnackbarAction? = null
) {
    showSnackbar(context.getString(snackbarText), timeLength, snackbarAction)
}

fun View.showSnackbar(
    snackbarText: String,
    timeLength: Int = Snackbar.LENGTH_LONG,
    snackbarAction: BkSnackbarAction? = null
) {
    val snack = Snackbar.make(this, snackbarText, timeLength)
    snackbarAction?.let {
        snack.setAction(it.text, it.action)
        snack.setActionTextColor(ContextCompat.getColor(context, R.color.white))
    }
    snack.show()
}

data class BkSnackbarAction(@StringRes val text: Int, val action: (View) -> Unit)
