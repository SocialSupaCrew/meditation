package com.socialsupacrew.meditation.design

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.meditation.R

class CustomDividerItem(context: Context, orientation: Int) :
    DividerItemDecoration(context, orientation) {

    init {
        ContextCompat.getDrawable(context, R.drawable.list_divider)?.let { setDrawable(it) }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        // hide the divider for the last child
        parent.adapter?.let {
            if (position == it.itemCount - 1) {
                outRect.setEmpty()
            } else {
                super.getItemOffsets(outRect, view, parent, state)
            }
        }
    }
}
