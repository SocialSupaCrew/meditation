package com.socialsupacrew.meditation.design

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import com.google.android.material.button.MaterialButton
import com.socialsupacrew.meditation.R


class PlayButton(
    context: Context,
    attrs: AttributeSet
) : MaterialButton(context, attrs) {

    init {
        icon = context.getDrawable(R.drawable.ic_pause_black_24dp)
        iconSize = resources.getDimensionPixelSize(R.dimen.play_button_icon_size)
        iconTint = ColorStateList.valueOf(context.getColor(R.color.white))
        background = context.getDrawable(R.drawable.bg_play_button)

        resources.getDimensionPixelSize(R.dimen.play_button_size).run {
            height = this
            minHeight = this
            maxHeight = this
            width = this
            minWidth = this
            maxWidth = this
        }
    }
}
