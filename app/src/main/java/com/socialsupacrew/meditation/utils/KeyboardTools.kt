package com.socialsupacrew.meditation.utils

import android.app.Dialog
import android.content.Context
import android.os.IBinder
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity

object KeyboardTools {
    fun View.showKeyboardForced() {
        showKeyboard(null, Flag.FORCED)
    }

    fun View.showKeyboard(direction: Int? = null, flag: Flag = Flag.IMPLICIT) {
        val requestFocus = if (direction == null) {
            requestFocus()
        } else {
            requestFocus(direction)
        }
        if (requestFocus) context.getInputMethodManager().showSoftInput(this, flag.showFlag)
    }

    fun Dialog.showKeyboard() {
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    fun AppCompatActivity.hideKeyboardForced() {
        hideKeyboard(Flag.FORCED)
    }

    fun AppCompatActivity.hideKeyboard(flag: Flag = Flag.IMPLICIT) {
        hideKeyboard(currentFocus, flag)
    }

    fun AppCompatActivity.hideKeyboard(view: View?, flag: Flag = Flag.IMPLICIT) {
        if (view == null || view.windowToken == null) return
        hideKeyboard(flag, view.windowToken)
        view.clearFocus()
    }

    private fun AppCompatActivity.hideKeyboard(flag: Flag = Flag.IMPLICIT, windowToken: IBinder) {
        getInputMethodManager().hideSoftInputFromWindow(windowToken, flag.hideFlag)
    }

    fun isEnterEvent(actionId: Int, event: KeyEvent?): Boolean {
        return actionId == EditorInfo.IME_ACTION_DONE || event != null && event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER
    }

    private fun Context.getInputMethodManager(): InputMethodManager {
        return getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    enum class Flag constructor(internal val showFlag: Int, internal val hideFlag: Int) {
        IMPLICIT(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY),
        FORCED(InputMethodManager.SHOW_FORCED, 0)
    }
}
