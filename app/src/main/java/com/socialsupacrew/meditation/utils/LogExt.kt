package com.socialsupacrew.meditation.utils

import android.util.Log

private const val TAG = "QSD"

fun log(message: String, tag: String = TAG, throwable: Throwable? = null) {
    Log.d(tag, message, throwable)
}
